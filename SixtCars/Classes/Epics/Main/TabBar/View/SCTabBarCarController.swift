//
//  SCTabBarController.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import UIKit

protocol ITabBarCarControllerItem {
    func configure(service: ICarService)
}

class SCTabBarCarController: UITabBarController {
    
    enum CntrollerType: Int {
        case map
        case cars
        
        var localized: String {
            switch self {
            case .map:
                return "map".localized
            case .cars:
                return "cars".localized
            }
        }
        
    }
    
    private let viewModel = ViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureControllers()
    }
    
    //MARK: - Private funcs
    
    private func configureControllers() {
        
        self.viewControllers?.enumerated().forEach({ (index, vc) in
            let navController = vc as? UINavigationController
            let item = navController?.viewControllers.first as? ITabBarCarControllerItem
            item?.configure(service: self.viewModel.service)
            self.tabBar.items?[index].title = CntrollerType(rawValue: index)?.localized
        })
   
    }
    
}
