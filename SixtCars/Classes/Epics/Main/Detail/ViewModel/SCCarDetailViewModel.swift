//
//  SCCarDetailViewModel.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/20/20.
//

import Foundation

extension SCCarDetailViewController {
    
    class ViewModel {
        
        private(set) var car: SCCar?
        
        func configure(car: SCCar) {
            self.car = car
        }
        
    }
    
}

protocol ICarDetailCellDisplayModel {
    var identifier: SCCarDetailViewController.DisplayModel.ReuseIdentifier { get }
}

extension SCCarDetailViewController {
    
    struct DisplayModel {
        
        enum ReuseIdentifier: String {
            case info = "CarDetailInfoCell"
            case progress = "CarProgressCell"
        }
        
        struct InfoCell: ICarDetailCellDisplayModel {
            var identifier = ReuseIdentifier.info
            var title: String?
            var subtitle: String?
        }
        
        struct ProgressCell: ICarDetailCellDisplayModel {
            var identifier = ReuseIdentifier.progress
            var title: String?
            var progress: Float
        }
        
        let items: [ICarDetailCellDisplayModel]
        let imageUrl: String?
        let title: String?
        let subtitle: String?
        let showMapInfo: Bool
        
        
        init(car: SCCar, showMapInfo: Bool) {
            
            self.title = car.modelName
            self.subtitle = car.name
            self.imageUrl = car.imageUrl
            self.showMapInfo = showMapInfo
            
            let make = InfoCell(title: "make_by".localized, subtitle: car.make)
            let group = InfoCell(title: "group".localized, subtitle: car.group)
            let series = InfoCell(title: "series".localized, subtitle: car.series)
            
            let transmission = InfoCell(title: "transmission".localized, subtitle: car.transmission.localized)
            let licensePlate = InfoCell(title: "license_plate".localized, subtitle: car.licensePlate)
            
            let fuelType = InfoCell(title: "fuel_type".localized, subtitle: car.fuelType.localized)
            let fuelLevl = ProgressCell(title: "fuel_level".localized, progress: car.fuelLevel)
                        
            self.items = [make, group, series, transmission, licensePlate, fuelType, fuelLevl]
            
        }
        
        static func generateMapObject(car: SCCar) -> SCMapViewController.MapObject {
            return SCMapViewController.MapObject(identifier: car.id,
                      title: car.modelName,
                      subtitle: car.name,
                      imageUrl: car.imageUrl,
                      latitude: car.latitude,
                      longitude: car.longitude)
        }
        
        
    }

}

