//
//  SCCarDetailViewController.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/20/20.
//

import UIKit

protocol ICarDetailCell: UITableViewCell {
    func configure(model: ICarDetailCellDisplayModel)
}

class SCCarDetailViewController: UIViewController {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet private weak var imageHeightConstraint: NSLayoutConstraint!
    
    private let imageAspectRatio: CGFloat = 60 / 105
    private var viewModel = ViewModel()
    private var displayModel: DisplayModel?
    
    //MARK: - Override funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.reloadData()
        self.updateContentInset()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let car = sender as? SCCar, segue.identifier == "map" {
            let mapObject = DisplayModel.generateMapObject(car: car)
            (segue.destination as? SCMapViewController)?.configure(objects: [mapObject], zoomLocation: mapObject.coordinate)
        }
    }
    
    //MARK: - User actions
    
    @IBAction func didSelectMabButton(_ sender: Any) {
        guard let car = self.viewModel.car else {
            return
        }
        self.performSegue(withIdentifier: "map", sender: car)
    }
    
    //MARK: - Public funcs
    
    func configure(showMapInfo: Bool, car: SCCar) {
        self.viewModel.configure(car: car)
        let displayModel = DisplayModel(car: car, showMapInfo: showMapInfo)
        self.configure(displayModel: displayModel)
    }

    //MARK: - Private funcs
    
    private func configure(displayModel: DisplayModel) {
        self.displayModel = displayModel
        if self.isViewLoaded {
            self.reloadData()
        }
    }
    
    private func reloadData() {
        self.tableView.reloadData()
        self.imageView.setImage(imageUrlStr: self.displayModel?.imageUrl)
        self.titleLabel.text = self.displayModel?.title
        self.subtitleLabel.text = self.displayModel?.subtitle
        self.mapButton.isHidden = !(self.displayModel?.showMapInfo ?? false)
    }
    
    private func updateContentInset() {
        var contentInset = self.tableView.contentInset
        contentInset.top = self.view.bounds.width * self.imageAspectRatio
        contentInset.bottom = 50
        self.tableView.contentInset = contentInset
        
        var offSet = self.tableView.contentOffset
        offSet.y = -contentInset.top
        self.tableView.contentOffset = offSet
    }
    
}

extension SCCarDetailViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.displayModel?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = self.displayModel!.items[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: item.identifier.rawValue, for: indexPath)
        (cell as? ICarDetailCell)?.configure(model: item)
        return cell
    }
    
}

extension SCCarDetailViewController: UITableViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let height = self.view.bounds.width * self.imageAspectRatio
        let offset = scrollView.contentOffset.y + scrollView.contentInset.top
        let newHeight = height - offset
        self.imageHeightConstraint.constant = newHeight
    }
    
}
