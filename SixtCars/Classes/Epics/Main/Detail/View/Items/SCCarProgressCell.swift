//
//  SCCarProgressCell.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/20/20.
//

import UIKit

class SCCarProgressCell: UITableViewCell, ICarDetailCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var progressView: UIProgressView!
    
    func configure(model: ICarDetailCellDisplayModel) {
        guard let model = model as? SCCarDetailViewController.DisplayModel.ProgressCell else {
            return
        }
        self.titleLabel.text = model.title
        self.progressView.progress = model.progress
    }

}
