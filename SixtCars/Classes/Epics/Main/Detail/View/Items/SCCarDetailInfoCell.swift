//
//  SCCarDetailInfoCell.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/20/20.
//

import UIKit

class SCCarDetailInfoCell: UITableViewCell, ICarDetailCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!

    func configure(model: ICarDetailCellDisplayModel) {
        guard let model = model as? SCCarDetailViewController.DisplayModel.InfoCell else {
            return
        }
        self.titleLabel.text = model.title
        self.subtitleLabel.text = model.subtitle
    }
    
}
