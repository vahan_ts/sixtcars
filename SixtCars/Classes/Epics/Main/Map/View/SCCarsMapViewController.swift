//
//  SCCarsMapViewController.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import UIKit
import MapKit

protocol ICarsMapView: class {
    func update(cars: [SCCar])
}

class SCCarsMapViewController: SCMapViewController {
    
    class MapButton: UIButton {
        var identifier: String?
    }

    private let viewModel = ViewModel()
    
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    //MARK: - Override funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.viewModel.view = self
        self.getData()
    }
    
    override func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let annotationView = super.mapView(mapView, viewFor: annotation)
        let infoButton = MapButton(type: .detailDisclosure)
        infoButton.identifier = (annotation as? MapObject)?.identifier
        infoButton.addTarget(self, action: #selector(didSelectCarInfoButton(button:)), for: .touchUpInside)
        annotationView?.rightCalloutAccessoryView = infoButton
        return annotationView
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let car = sender as? SCCar, segue.identifier == "CarDetail" {
            (segue.destination as? SCCarDetailViewController)?.configure(showMapInfo: false, car: car)
        }
    }
    
    //MARK: - Private funcs
    
    private func getData() {
        self.activityIndicatorView.startAnimating()
        self.viewModel.get { [weak self] (_) in
            self?.activityIndicatorView.stopAnimating()
        } failure: { [weak self] (error) in
            self?.activityIndicatorView.stopAnimating()
            self?.showErrorAlert(error: error)
        }
    }
    
    private func showErrorAlert(error: IError) {
        let alert = UIAlertController(title: "warning".localized, message: error.message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok".localized, style: .cancel, handler: nil))
        
        let reload = UIAlertAction(title: "reload".localized, style: .default) { (_) in
            self.getData()
        }
        alert.addAction(reload)
        self.present(alert, animated: true)
    }
    
    @objc private func didSelectCarInfoButton(button: MapButton) {
        guard let identifier = button.identifier, let car = self.viewModel.car(for: identifier) else {
            return
        }
        self.performSegue(withIdentifier: "CarDetail", sender: car)
    }

}


extension SCCarsMapViewController: ICarsMapView {
    
    func update(cars: [SCCar]) {
        let displayModel = DisplayModel(cars: cars)
        self.configure(objects: displayModel.mapObjects, zoomLocation: displayModel.zoomLocation)
    }
    
}

extension SCCarsMapViewController: ITabBarCarControllerItem {
    
    func configure(service: ICarService) {
        self.viewModel.configure(service: service)
    }
    
}
