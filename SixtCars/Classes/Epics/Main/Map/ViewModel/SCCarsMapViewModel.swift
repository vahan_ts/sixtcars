//
//  SCCarsMapViewModel.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import MapKit

extension SCCarsMapViewController {
    
    class ViewModel: SCCarServiceObserver {
        
        weak var service: ICarService?
        weak var view: ICarsMapView?
        private(set) var cars: [SCCar]?
        
        func configure(service: ICarService) {
            self.service?.remove(observer: self)
            self.service = service
            self.service?.add(observer: self)
        }
        
        func get(success: (@escaping (Bool) -> Void), failure: @escaping ((SCNetworkError)  -> Void)) {
            guard let service = self.service else {
                failure(SCNetworkError.type(type: .conflict))
                return
            }
            service.getCars(forceRefresh: false, success: { [weak self] (cars, loadFromServer) in
                success(true)
                if !loadFromServer {
                    self?.view?.update(cars: cars)
                }
            }, failure: failure)
        }
        
        func didReceive(carService: ICarService, cars: [SCCar]) {
            self.cars = cars
            self.view?.update(cars: cars)
        }
        
        func car(for id: String) -> SCCar? {
            return self.cars?.first(where: { $0.id == id })
        }
        
    }
    
}

extension SCCarsMapViewController {
    
    struct DisplayModel {
        
        let mapObjects: [MapObject]
        let zoomLocation: CLLocationCoordinate2D?
        
        init(cars: [SCCar]) {
            self.mapObjects = cars.map { (car) -> MapObject in
                return MapObject(identifier: car.id,
                          title: car.modelName,
                          subtitle: car.name,
                          imageUrl: car.imageUrl,
                          latitude: car.latitude,
                          longitude: car.longitude)
            }
            self.zoomLocation = self.mapObjects.first?.coordinate
        }
        
    }

}
