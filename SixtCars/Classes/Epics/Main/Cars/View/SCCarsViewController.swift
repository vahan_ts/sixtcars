//
//  SCCarsViewController.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/21/20.
//

import UIKit

protocol ICarsView: class {
    func update(cars: [SCCar])
}

class SCCarsViewController: UIViewController {

    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet private weak var tableView: UITableView!
    private var refreshControl = UIRefreshControl()
    
    private let viewModel = ViewModel()
    private var displayModel: DisplayModel?
    
    private let cellIdentifier = "cellID"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "cars".localized
        
        self.refreshControl.addTarget(self, action: #selector(refreshCarsData), for: .valueChanged)
        self.tableView.addSubview(refreshControl)
        
        self.viewModel.view = self
        self.getData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detail", let cell = sender as? UITableViewCell, let indexPath = self.tableView.indexPath(for: cell), let item = self.displayModel?.items[indexPath.row], let car = self.viewModel.car(for: item.id) {
            (segue.destination as? SCCarDetailViewController)?.configure(showMapInfo: true, car: car)
        }
    }
    
    //MARK: - Private funcs
    
    private func getData() {
        self.activityIndicatorView.startAnimating()
        self.viewModel.get { [weak self] (_) in
            self?.activityIndicatorView.stopAnimating()
        } failure: { [weak self] (error) in
            self?.activityIndicatorView.stopAnimating()
            self?.showErrorAlert(error: error)
        }
    }
    
    @objc private func refreshCarsData() {
        self.activityIndicatorView.startAnimating()
        self.viewModel.refresh { [weak self] (_) in
            self?.activityIndicatorView.stopAnimating()
            self?.refreshControl.endRefreshing()
        } failure: { [weak self] (error) in
            self?.activityIndicatorView.stopAnimating()
            self?.showErrorAlert(error: error)
            self?.refreshControl.endRefreshing()
        }
    }
    
    private func showErrorAlert(error: IError) {
        let alert = UIAlertController(title: "warning".localized, message: error.message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok".localized, style: .cancel, handler: nil))
        
        let reload = UIAlertAction(title: "reload".localized, style: .default) { (_) in
            self.getData()
        }
        alert.addAction(reload)
        self.present(alert, animated: true)
    }
        
}

extension SCCarsViewController: ICarsView {
    
    func update(cars: [SCCar]) {
        self.displayModel = DisplayModel(cars: cars)
        guard self.isViewLoaded else {
            return
        }
        self.tableView.reloadData()
    }
    
}


extension SCCarsViewController: ITabBarCarControllerItem {
    
    func configure(service: ICarService) {
        self.viewModel.configure(service: service)
    }
    
}

extension SCCarsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.displayModel?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath)
        let item = self.displayModel?.items[indexPath.row]
        (cell as? SCCarsTableViewCell)?.configure(model: item)
        return cell
    }
    
}

