//
//  SCCarsTableViewCell.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/21/20.
//

import UIKit

class SCCarsTableViewCell: UITableViewCell {

    @IBOutlet private weak var carImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var subtitleLabel: UILabel!
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)
        self.alpha = highlighted ? 0.6 : 1
    }
        
    func configure(model: SCCarsViewController.DisplayModel.Item?) {
        self.carImageView.setImage(imageUrlStr: model?.carImageUrl)
        self.titleLabel.text = model?.title
        self.subtitleLabel.text = model?.subtitleL
    }
    
}
