//
//  SCCarsViewModel.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/21/20.
//

import Foundation

extension SCCarsViewController {
    
    class ViewModel: SCCarServiceObserver {
        
        weak var service: ICarService?
        weak var view: ICarsView?
        private(set) var cars: [SCCar]?
        
        func configure(service: ICarService) {
            self.service?.remove(observer: self)
            self.service = service
            self.service?.add(observer: self)
        }
        
        func get(success: (@escaping (Bool) -> Void), failure: @escaping ((SCNetworkError)  -> Void)) {
            guard let service = self.service else {
                failure(SCNetworkError.type(type: .conflict))
                return
            }
            service.getCars(forceRefresh: false, success: { [weak self] (cars, loadFromServer) in
                success(true)
                if !loadFromServer {
                    self?.view?.update(cars: cars)
                }
            }, failure: failure)
        }
        
        func refresh(success: (@escaping (Bool) -> Void), failure: @escaping ((SCNetworkError)  -> Void)) {
            guard let service = self.service else {
                failure(SCNetworkError.type(type: .conflict))
                return
            }
            service.getCars(forceRefresh: true, success: { (_,_) in
                success(true)
            }, failure: failure)
        }
                
        func didReceive(carService: ICarService, cars: [SCCar]) {
            self.cars = cars
            self.view?.update(cars: cars)
        }
        
        func car(for id: String) -> SCCar? {
            return self.cars?.first(where: { $0.id == id })
        }
        
    }
    
    struct DisplayModel {
        
        struct Item {
            let id: String
            let carImageUrl: String?
            let title: String?
            let subtitleL: String?
        }
        
        let items: [Item]
        
        init(cars: [SCCar]) {
            self.items = cars.map { (car) -> Item in
                return Item(id: car.id,
                            carImageUrl: car.imageUrl,
                            title: car.name,
                            subtitleL: car.modelName)
            }
        }
        
    }
    
}
