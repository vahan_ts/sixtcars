//
//  SCCar.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import Foundation

class SCCar: Decodable {
    
    enum Transmission: String, Decodable {
        case manual = "M"
        case automatic = "A"
        
        var localized: String {
            switch self {
            case .manual:
                return "manual".localized
            case .automatic:
                return "automatic".localized
            }
        }
    }
    
    enum FuelType: String, Decodable {
        case diesel = "D"
        case petrol = "P"
        case electric = "E"
        
        var localized: String {
            switch self {
            case .diesel:
                return "diesel".localized
            case .petrol:
                return "petrol".localized
            case .electric:
                return "electric".localized
            }
        }
    }
    
    enum Cleanliness: String, Decodable {
        case clean = "CLEAN"
        case veryVlean = "VERY_CLEAN"
        case regular = "REGULAR"
        
        var localized: String {
            switch self {
            case .clean:
                return "clean".localized
            case .veryVlean:
                return "very_clean".localized
            case .regular:
                return "regular".localized
            }
        }
    }
    
    private enum CodingKeys: String, CodingKey {
        case id = "id"
        case modelIdentifier = "modelIdentifier"
        case modelName = "modelName"
        case name = "name"
        case make = "make"
        case group = "group"
        case color = "color"
        case series = "series"
        case fuelType = "fuelType"
        case fuelLevel = "fuelLevel"
        case transmission = "transmission"
        case licensePlate = "licensePlate"
        case latitude = "latitude"
        case longitude = "longitude"
        case innerCleanliness = "innerCleanliness"
        case imageUrl = "carImageUrl"
    }

    var id: String
    var modelIdentifier: String
    var modelName: String
    var name: String
    var make: String
    var group: String
    var color: String
    var series: String
    var fuelType: FuelType
    var fuelLevel: Float
    var transmission: Transmission
    var licensePlate: String
    var latitude: Double
    var longitude: Double
    var innerCleanliness: Cleanliness?
    var imageUrl: String?
        
}
