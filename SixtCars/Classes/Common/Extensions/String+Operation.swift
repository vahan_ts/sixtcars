//
//  String+Operation.swift
//  CriptoBittrex
//
//  Created by Vahan Tsogolakyan on 12/12/20.
//

import Foundation

extension String {
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, comment: "")
    }
    
}
