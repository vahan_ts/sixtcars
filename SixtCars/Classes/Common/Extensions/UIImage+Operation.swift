//
//  UIImage+Operation.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/20/20.
//

import UIKit

extension UIImage {
    
    func resized(to size: CGSize) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        self.draw(in: CGRect(origin: .zero, size: size))
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func resized(to width: CGFloat) -> UIImage? {
        let sizeImage = CGSize(width: width, height: width/size.width * size.height)
        return resized(to: sizeImage)
    }
}
