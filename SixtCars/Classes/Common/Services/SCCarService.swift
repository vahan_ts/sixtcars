//
//  SCCarService.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import Foundation

protocol SCCarServiceObserver {
    func didReceive(carService: ICarService, cars: [SCCar])
    func didReceive(carService: ICarService, error: IError)
}

extension SCCarServiceObserver {
    func didReceive(carService: ICarService, cars: [SCCar]) {}
    func didReceive(carService: ICarService, error: IError) {}
}

protocol ICarService: class {
    func getCars(forceRefresh: Bool, success: (@escaping (_ cars: [SCCar], _ loadFromServer: Bool) -> Void), failure: @escaping ((SCNetworkError)  -> Void))
    func add(observer: SCCarServiceObserver)
    func remove(observer: SCCarServiceObserver)
}

class SCCarService {
    
    private let observer = SCObserver<SCCarServiceObserver>()
    private let carsWorker = SCCodingTaskCarsWorker()
    private var cars: [SCCar]?
            
    //MARK: - Private funcs
    
    private func getCars(success: (@escaping ([SCCar]) -> Void), failure: @escaping ((SCNetworkError)  -> Void)) {
        self.carsWorker.getCars { [weak self] (cars) in
            success(cars)
            self?.cars = cars
            self?.didReceive(cars: cars)
        } failure: { (error) in
            failure(error)
            self.didReceive(error: error)
        }
    }
    
    private func didReceive(cars: [SCCar]) {
        self.observer.forEach { (observer) in
            observer.didReceive(carService: self, cars: cars)
        }
    }
    
    private func didReceive(error: IError) {
        self.observer.forEach { (observer) in
            observer.didReceive(carService: self, error: error)
        }
    }
    
}

extension SCCarService: ICarService {
    
    func getCars(forceRefresh: Bool, success: (@escaping (_ cars: [SCCar], _ loadFromServer: Bool) -> Void), failure: @escaping ((SCNetworkError)  -> Void)) {
        guard let cars = self.cars, !forceRefresh else {
            self.getCars(success: { (cars) in
                success(cars, true)
            }, failure: failure)
            return
        }
        success(cars, false)
    }
    
    func add(observer: SCCarServiceObserver) {
        self.observer.addObject(observer)
    }
    
    func remove(observer: SCCarServiceObserver) {
        self.observer.removeObject(observer)
    }
    
}
