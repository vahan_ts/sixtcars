//
//  SCImageCache.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/20/20.
//

import UIKit
import Kingfisher

protocol SCImageCacheDelegate: class {
    func didCache(imageCache: SCImageCache, image: SCImageCache.Image, cacheImage: UIImage)
}

class SCImageCache {
    
    struct Image {
        let identifier: String
        let url: String
        let defaultImage: UIImage?
        let imageWidth: CGFloat?
        
        fileprivate var cacheKey: String {
            if let imageWidth = self.imageWidth {
                return self.url + "&imageWidth=\(imageWidth)"
            }
            return self.url
        }
    }
    
    static private let imageCache = NSCache<AnyObject, UIImage>()
    private let images: [Image]
    
    weak var delegate: SCImageCacheDelegate?
    
    init(images: [Image]) {
        self.images = images
        self.startDownload()
    }
    
    func imageFor(identifier: String) -> UIImage? {
        guard let image = self.images.first(where: { $0.identifier == identifier}) else {
            return nil
        }
        return  Self.imageCache.object(forKey: (image.cacheKey as NSString)) ?? image.defaultImage
    }
    
    //MARK: Private
    
    private func startDownload() {
        self.images.forEach { (image) in
            self.download(image: image)
        }
                
    }
    
    private func download(image: Image) {
        guard let url = URL(string: image.url), Self.imageCache.object(forKey: (image.cacheKey as NSString)) == nil else {
            return
        }
        ImageDownloader.default.downloadImage(with: url, options: .none, completionHandler:  { [weak self] (result) in
            switch result {
            case .success(let success):
                self?.didEndDownload(image: image, downloadedImage: success.image)
            case .failure(_):
                break
            }
        })
    }
    
    private func didEndDownload(image: Image, downloadedImage: UIImage) {
        var downloadedImage = downloadedImage
        if let imageWidth = image.imageWidth, let resizeImage = downloadedImage.resized(to: imageWidth) {
            downloadedImage = resizeImage
        }
        Self.imageCache.setObject(downloadedImage, forKey: image.cacheKey as NSString)
        self.delegate?.didCache(imageCache: self, image: image, cacheImage: downloadedImage)
    }
    
}
