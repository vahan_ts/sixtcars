//
//  SCError.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import Foundation

protocol IError: Error {
    var message: String { get }
}
