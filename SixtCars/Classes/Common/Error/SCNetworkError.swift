//
//  SCNetworkError.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import Alamofire

enum SCNetworkError: IError {
    
    case type(type: Type)
    case error(error: Error, statusCode: Int?)
    
    var message: String {
        switch self {
        case .type(let type):
            return type.message
        case .error(let error, _):
            return error.localizedDescription
        }
    }
    
    var statusCode: Int{
        switch self {
        case .type(let type):
            return type.rawValue
        case .error(_, let statusCode):
            if let statusCode = statusCode {
                return statusCode
            }
            return self.type.rawValue
        }
    }
    
    var type: Type {
        switch self {
        case .type(let type):
            return type
        case .error(let error, let statusCode):
            if let statusCode = statusCode {
                if let type = Type(rawValue: statusCode) {
                    return type
                } else {
                    return Type.unknownError
                }
            } else {
                if let error = error.asAFError, error.isExplicitlyCancelledError {
                    return Type.cancelled
                } else {
                    return Type.unknownError
                }
            }
        }
    }
    
    var noInternetConnection: Bool {
        return self.type == .noInternetConnection
    }
    
    var isCancelled: Bool {
        return self.type == .cancelled
    }
    
    var noExistingData: Bool {
        return self.type == .notFound
    }
}

extension SCNetworkError {
    
    enum `Type`: Int {
        
        case badRequest = 400
        case unauthorized = 401
        case notFound = 404
        case serverError = 500
        case conflict = 409
        case forbidden = 403
        case notImplemented = 501
        
        case cancelled
        case requestTimeOut
        case noInternetConnection
        case unknownError

        var message: String {
            switch self {
            case .badRequest:
                return "network_error_bad_request".localized
            case .unauthorized:
                return "network_error_unauthorized".localized
            case .notFound:
                return "network_error_not_found".localized
            case .serverError:
                return "network_error_server".localized
            case .conflict:
                return "network_error_conflict".localized
            case .forbidden:
                return "network_error_forbidden".localized
            case .notImplemented:
                return "network_error_not_implemented".localized
            case .cancelled:
                return "network_error_cancelled".localized
            case .requestTimeOut:
                return "network_error_request_time_out".localized
            case .noInternetConnection:
                return "network_error_no_connection".localized
            case .unknownError:
                return "nework_error_unknown_error".localized
            }
        }
        
    }
    
}
