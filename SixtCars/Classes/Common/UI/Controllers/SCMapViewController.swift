//
//  SCMapViewController.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import UIKit
import MapKit

extension SCMapViewController {
    
    class MapObject: NSObject, MKAnnotation {
       
        var identifier: String
        let title: String?
        let subtitle: String?
        let imageUrl: String?
        var latitude: Double
        var longitude: Double
        
        init(identifier: String, title: String?, subtitle: String?, imageUrl: String?, latitude: Double, longitude: Double) {
            self.identifier = identifier
            self.title = title
            self.subtitle = subtitle
            self.imageUrl = imageUrl
            self.latitude = latitude
            self.longitude = longitude
        }
        
        lazy var coordinate: CLLocationCoordinate2D = {
            return CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
        }()
        
    }
    
}

class SCMapViewController: UIViewController, MKMapViewDelegate, SCImageCacheDelegate {
        
    @IBOutlet private(set) weak var mapView: MKMapView!
    private var mapObjects: [MapObject]?
    private let reuseIdentifier = "MKAnnotationViewIdentifier"
    private var imageCache: SCImageCache?
    private var zoomLocation: CLLocationCoordinate2D?
    
    //MARK: - Override funcs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "map".localized
        self.tabBarItem.title = "map".localized
        self.mapView.register(MKAnnotationView.self, forAnnotationViewWithReuseIdentifier: self.reuseIdentifier)
        self.mapView.delegate = self
        self.configure(objects: self.mapObjects, zoomLocation: self.zoomLocation)
    }
    
    //MARK: - Public funcs
    
    func configure(objects: [MapObject]?, zoomLocation: CLLocationCoordinate2D?) {
        guard self.isViewLoaded else {
            self.mapObjects = objects
            self.zoomLocation = zoomLocation
            return
        }
        
        if let zoomLocation = zoomLocation {
            self.zoom(in: zoomLocation)
        }
        
        let defaultImage = UIImage(named: "icon_map_car")
        
        let imagesCache = objects?.map({ object -> SCImageCache.Image in
            return SCImageCache.Image(identifier: object.identifier,
                                      url: (object.imageUrl ?? ""),
                                      defaultImage: defaultImage,
                                      imageWidth: 22 * UIScreen.main.scale)
        }) ?? []
        
        self.imageCache = SCImageCache(images: imagesCache)
        self.imageCache?.delegate = self
        self.mapView.removeAnnotations(self.mapView.annotations)
        guard let objects = objects else {
            return
        }
        self.mapView.addAnnotations(objects)
    }
    
    func mapObject(for identifier: String) -> MapObject? {
        return self.mapView.annotations.first(where: { ($0 as? MapObject)?.identifier == identifier }) as? MapObject
    }
    
    //MARK: - MKMapViewDelegate
        
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let annotation = annotation as? MapObject else {
            return nil
        }
        let annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: self.reuseIdentifier, for: annotation)
        annotationView.image = self.imageCache?.imageFor(identifier: annotation.identifier)
        annotationView.canShowCallout = true
        return annotationView
    }
        
    func zoom(in location: CLLocationCoordinate2D)  {
        let annotationPoint: MKMapPoint =  MKMapPoint(location)
        let size = MKMapSize(width: 20000, height: 20000)
        let zoomRect:MKMapRect = MKMapRect(x: annotationPoint.x - size.width/2, y: annotationPoint.y - size.height/2, width: size.width, height: size.height)
        self.mapView.setVisibleMapRect(zoomRect, animated: true)
    }
    
    //MARK: - SCImageCacheDelegate
    
    func didCache(imageCache: SCImageCache, image: SCImageCache.Image, cacheImage: UIImage) {
        guard let annotation = self.mapView.annotations.first(where: { ($0 as? MapObject)?.identifier == image.identifier }) else {
            return
        }
        self.mapView.view(for: annotation)?.image = cacheImage
    }

}
