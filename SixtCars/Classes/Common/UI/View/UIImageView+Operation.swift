//
//  UIImageView+Operation.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/21/20.
//

import Kingfisher

extension UIImageView {
        
    func setImage(imageUrlStr: String?, placeholder: UIImage? = nil) {
        if let imageUrlStr = imageUrlStr {
            let url = URL(string: imageUrlStr)
            self.setImage(url: url, placeholder: placeholder)
        } else {
            let url: URL? = nil
            self.setImage(url: url, placeholder: placeholder)
        }
    }
    
    func setImage(url: URL?, placeholder: UIImage? = nil) {
        self.kf.setImage(with: url, placeholder: placeholder)
    }
}
