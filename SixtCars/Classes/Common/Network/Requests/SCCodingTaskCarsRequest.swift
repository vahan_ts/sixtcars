//
//  SCCodingTaskCarsRequest.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import Alamofire

enum SCCodingTaskCarsRequest {
    case getCars
}

extension SCCodingTaskCarsRequest: SCRequest {
   
    var path: String {
        switch self {
        case .getCars:
            return "codingtask/cars"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getCars:
            return .get
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .getCars:
            return nil
        }
    }
    
    var parameters: [String : Any]? {
        switch self {
        case .getCars:
            return nil
        }
    }
    
    var encoding: ParameterEncoding {
        switch self {
        case .getCars:
            return JSONEncoding.default
        }
    }
    
}
