//
//  IRequest.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import Alamofire

//This defines the type of HTTP method used to perform the request
enum HTTPMethod: String {
    case get     = "GET"
    case post    = "POST"
    case put     = "PUT"
    case patch   = "PATCH"
    case delete  = "DELETE"
}

//This is the Request protocol you may implement other classes can conform

protocol IRequest {
    var url: String { get }
    var method: HTTPMethod { get }
    var headers: [String: String]? { get }
    var parameters: [String: Any]? { get }
    var encoding: ParameterEncoding { get }
    var queryItems: [String: String]? { get }
}

// AF Extension
extension IRequest {
    
    var afHeaders: Alamofire.HTTPHeaders? {
        if let header = self.headers {
            return Alamofire.HTTPHeaders(header)
        }
        return nil
    }
    
    var queryItems: [String: String]? {
        return nil
    }
    
}

extension HTTPMethod {
    
    var AFMethod: Alamofire.HTTPMethod  {
        switch self {
        case .get:
            return Alamofire.HTTPMethod.get
        case .post:
            return Alamofire.HTTPMethod.post
        case .put:
            return Alamofire.HTTPMethod.put
        case .patch:
            return Alamofire.HTTPMethod.patch
        case .delete:
            return Alamofire.HTTPMethod.delete
        }
    }
    
}

//MARK: - Environment

struct SCEnvironment {
    static let baseUrl = "https://cdn.sixt.io/"
}

//MARK: - SCRequest Request

protocol SCRequest: IRequest {
    var path: String { get }
}

extension SCRequest {
    
    var url: String {
        return SCEnvironment.baseUrl + self.path
    }
    
}

