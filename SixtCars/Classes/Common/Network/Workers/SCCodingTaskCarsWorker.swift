//
//  SCCodingTaskCarsWorker.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import Alamofire

class SCCodingTaskCarsWorker: SCWorker {
    
    @discardableResult func getCars(success: (@escaping ([SCCar]) -> Void), failure: ((SCNetworkError)  -> Void)? = nil) -> Request? {
        let request = SCCodingTaskCarsRequest.getCars
        return self.request(type: [SCCar].self, request: request, success: success, failure: failure)
    }
    
}


