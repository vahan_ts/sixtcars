//
//  SCWorker.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import Alamofire

class SCWorker {
    
    private let dispatcher = SCDispatcher.sheared
    
    @discardableResult func request<T: Decodable>(type: T.Type, request: IRequest, success: (@escaping (T) -> Void), failure: ((SCNetworkError)  -> Void)? = nil) -> Request? {
        self.dispatcher.request(type: type, request: request, success: success, failure: failure)
    }
    
}
