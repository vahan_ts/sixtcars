//
//  SCDispatcher.swift
//  SixtCars
//
//  Created by Vahan Tsogolakyan on 12/19/20.
//

import Alamofire

class SCDispatcher {
    
    typealias Success<T> = ((T) -> Void)
    typealias Failure = ((SCNetworkError) -> Void)
    
    static let sheared: SCDispatcher = SCDispatcher()
   
    private init() {}
        
    //MARK: - public
    
    func request(request: IRequest, success: @escaping Success<Any>, failure: Failure? = nil) -> Request? {
        do {
            let dataRequest = try self.dataRequest(request: request).responseJSON {(response) in
                switch response.result {
                case .failure(let error):
                    let networkError = SCNetworkError.error(error: error, statusCode: response.response?.statusCode)
                    failure?(networkError)
                case .success(let value):
                    do {
                        try Self.checkResponse(statusCode: response.response?.statusCode, error: nil)
                    } catch  {
                        failure?(error as! SCNetworkError)
                        return
                    }
                    success(value)
                }
            }
            return dataRequest
        } catch {
            failure?(error as! SCNetworkError)
            return nil
        }
    }
    
    func request<T: Decodable>(type: T.Type, request: IRequest, success: @escaping Success<T>, failure: Failure? = nil) -> Request? {
        do {
            let dataRequest = try self.dataRequest(request: request).responseDecodable {(response: AFDataResponse<T>) in
                switch response.result {
                case .failure(let networkError):
                    let error = SCNetworkError.error(error: networkError, statusCode: response.response?.statusCode)
                    failure?(error)
                case .success(let value):
                    do {
                        try Self.checkResponse(statusCode: response.response?.statusCode, error: nil)
                    } catch  {
                        failure?(error as! SCNetworkError)
                        return
                    }
                    success(value)
                }
            }
            return dataRequest
        } catch {
            failure?(error as! SCNetworkError)
            return nil
        }
    }
    
    //MARK: - private
    
    private func dataRequest(request: IRequest) throws -> DataRequest {
        
        let url = request.url
        
        guard var components = URLComponents(string: url) else {
            throw SCNetworkError.type(type: .conflict)
        }
        
        if let queryItems = request.queryItems {
            var query: [URLQueryItem] = components.queryItems ?? []
            for item in queryItems {
                let queryItem = URLQueryItem(name: item.key, value: item.value)
                query.append(queryItem)
            }
            components.queryItems = query
        }

        return AF.request(components, method: request.method.AFMethod, parameters: request.parameters, encoding: request.encoding, headers: request.afHeaders, interceptor: nil).validate(statusCode: 200..<300)
    }
    
    private static func checkResponse(statusCode: Int?, error: Error?) throws {
        if let error = error {
            throw SCNetworkError.error(error: error, statusCode: statusCode)
        }
    }
    
}
